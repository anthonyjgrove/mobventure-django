from django.conf.urls import patterns, include, url
from django.contrib import admin
import prereg.views as preregviews
from django.views.generic import TemplateView

urlpatterns = patterns('',

 url(r'^$', preregviews.LandingView.as_view()),
 url(r'share', preregviews.ShareView.as_view()),

)