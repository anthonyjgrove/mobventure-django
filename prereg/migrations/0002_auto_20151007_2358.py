# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prereg', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='member',
            options={'verbose_name_plural': 'Members'},
        ),
        migrations.AlterField(
            model_name='member',
            name='join_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Date Joined'),
        ),
        migrations.AlterField(
            model_name='member',
            name='mem_email',
            field=models.CharField(max_length=65, verbose_name=b'Email'),
        ),
    ]
